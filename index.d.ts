type Type = Function | string

interface PropertyDefinition<T> {
  type?: Type;
  required?: boolean;
  length?: number | { min?: number; max?: number };
  size?: number | { min?: number; max?: number };
  enum?: string[];
  each?: Rule<T>;
  elements?: Rule<T>[];
  match?: RegExp;
  use?: { [key: string]: ValidationFunction };
  message?: string | MessageFunction | { [key: string]: string | MessageFunction };
  schema?: Schema<T>;
  properties?: SchemaDefinition<T>;
}

type Rule<T> = Type
    | PropertyDefinition<T>
    | SchemaDefinition<T>
    | Schema<T>
    | RuleArray

// Remove when ts 3.7 is released
interface RuleArray extends Array<Rule<any>> {}

type SchemaDefinition<T> = {
  [P in keyof T]?: Rule<T[P]>
}

interface ValidationFunction {
  (value: any, ctx: object, ...args: any[]): boolean;
}

interface TypecastFunction {
  (value: any): unknown;
}

interface MessageFunction {
  (path: string, ctx: object, ...args: any[]): string;
}

interface ValidationOptions {
  typecast?: boolean;
  strip?: boolean;
}

export class ValidationError extends Error {
  constructor(message: string, path: string);
  path: string;
  status: number;
  expose: boolean;
}

export default class Schema<T extends Record<string, any>> {
  constructor(definition?: SchemaDefinition<T>, options?: ValidationOptions);
  path(path: string, rules?: Rule<T>): Property<T>;
  assert(target: T, options?: ValidationOptions): void;
  validate(target: T, options?: ValidationOptions): ValidationError[];
  message(validator: string, message: string | MessageFunction): Schema<T>;
  message(messages: { [validator: string]: string | MessageFunction }): Schema<T>;
  validator(name: string, fn: ValidationFunction): Schema<T>;
  validator(validators: { [name: string]: ValidationFunction }): Schema<T>;
  typecaster(name: string, fn: TypecastFunction): Schema<T>;
  typecaster(typecasters: { [name: string]: TypecastFunction }): Schema<T>;
}

declare class Property<T> {
  constructor(name: string, schema: Schema<T>);
  message(messages: (string | MessageFunction) | { [validator: string]: string | MessageFunction }): Property<T>;
  schema(schema: Schema<T>): Property<T>;
  use(fns: { [name: string]: ValidationFunction }): Property<T>;
  required(bool?: boolean): Property<T>;
  type(type: Type): Property<T>;
  string(): Property<T>;
  number(): Property<T>;
  array(): Property<T>;
  date(): Property<T>;
  length(rule: number | { min?: number; max?: number }): Property<T>;
  size(rule: number | { min?: number; max?: number }): Property<T>;
  enum(enums: string[]): Property<T>;
  match(regexp: RegExp): Property<T>;
  each(rules: Rule<T>): Property<T>;
  elements(arr: Rule<T>[]): Property<T>;
  properties(props: SchemaDefinition<T>): Property<T>;
  path(path: string, rules?: Rule<T>): Schema<T>;
  typecast<T>(value: any): T;
  validate(value: any, ctx: object, path: string): ValidationError | null;
}
